<?php

return [
    'demo' => 'demo',
    'system_config_group_list' => [
        'basic' => ['title' => '基本配置', 'icon' => 'ios-medal', 'desc' => ''],
        'content' => ['title' => '内容配置', 'icon' => 'md-menu', 'desc' => ''],
        'user' => ['title' => '用户配置', 'icon' => 'ios-walk', 'desc' => ''],
        'system' => ['title' => '系统配置', 'icon' => 'ios-trophy-outline', 'desc' => ''],
    ],
];
